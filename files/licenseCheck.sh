#!/bin/bash -l

apk --no-cache --no-progress add bash curl util-linux jq
echo "Starting docker daemon"
bash bitbucket_repo/files/docker-engine.sh start
echo
echo "Downloading twistcli from Twistlock Console"
curl -k -u ${TL_USER}:${TL_PASS} --output twistcli https://${TL_CONSOLE}/api/v1/util/twistcli
chmod a+x twistcli
./twistcli coderepo scan --address https://$TL_CONSOLE -u $TL_USER -p $TL_PASS bitbucket_repo/license  
result=$(curl -k -u $TL_USER:$TL_PASS -H 'Content-Type: application/json' "https://$TL_CONSOLE/api/v1/coderepos-ci?limit=1&reverse=true&sort=scanTime"|jq '.[0].pass')


if [ "$result" == "true" ]; then
   echo "License check passed!"
   exit 0
else
   echo "License check failed!"
   exit 1
fi
